INSERT INTO  user (`id`, `full_name`) VALUES
	(1, 'Ivana Ivanovska'),
	(2, 'Filip Filipovski'),
	(3, 'Andrej Andreevski');
INSERT INTO type (`id`, `type_name`) VALUES
	(1, 'movie'),
	(2, 'tv series'),
	(3, 'podcast'),
	(4, 'webinar'),
	(5, 'book');
INSERT INTO tag (`id`, `tag_name`) VALUES
	(1, 'sitcom'),
	(2, 'self-help'),
	(3, 'habits'),
	(4, 'diet'),
	(5, 'nutrition'),
	(6, 'science'),
	(7, 'plant-based'),
	(8, 'healthy');
INSERT INTO content (`id`, `created_at`, `description`, `rating`, `title`, `updated_at`, `who_is_it_for`, `notification_id`, `type_id`, `user_id`) VALUES
	(1, '2020-12-18 14:38:56', 'this is for friends', 8, 'Friends', NULL, 'anyone who enjoys something something', NULL, 2, 2),
	(2, '2020-12-18 14:40:24', 'this id for himym', 4, 'How i met your mother', NULL, 'everyone', NULL, 2, 1),
	(3, '2020-12-18 14:41:14', 'this is for atomic habits', 6, 'Atomic habits', NULL, 'people who something ', NULL, 5, 1),
	(4, '2020-12-18 14:44:26', 'this is for how not to die', 7, 'How not to die', NULL, 'people', NULL, 5, 3);
INSERT INTO content_tag (`content_id`, `tag_id`) VALUES
	(2, 1),
	(3, 2),
	(3, 3),
	(1, 1),
	(4, 4),
	(4, 8),
	(4, 3),
	(4, 5),
	(4, 6),
	(4, 7);
INSERT INTO review (`id`, `comment`, `grade`, `contentr_id`, `userr_id`) VALUES
	(1, 'I love it', 8, 1, 3),
	(2, 'I don\'tlike it', 3, 4, 2),
	(3, 'Not my cup of tea', 4.7, 2, 2),
	(4, 'Very good', 7, 3, 1),
	(5, 'Comsi comsa', 5, 4, 1);