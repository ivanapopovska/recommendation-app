package com.example.recommendationapp.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String message;

    @JsonFormat(pattern = "yyyy-mm-dd")
    private Date createdAt;

    @ManyToOne
    @JsonIgnore
    private User userN;

    @OneToOne
    private Content content;

    @PrePersist
    protected  void onCreate(){
        this.createdAt = new Date();
    }

}
