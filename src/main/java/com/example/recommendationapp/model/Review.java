package com.example.recommendationapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private float grade;
    private String comment;

    @ManyToOne
    @JsonIgnore
    private User userR;

    @ManyToOne
    @JsonIgnore
    private Content contentR;


}
