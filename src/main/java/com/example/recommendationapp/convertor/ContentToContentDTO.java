package com.example.recommendationapp.convertor;

import com.example.recommendationapp.DTO.ContentDTO;
import com.example.recommendationapp.model.Content;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ContentToContentDTO implements Converter<Content, ContentDTO>{

    @Synchronized
    @Nullable
    @Override
    public ContentDTO convert(Content source) {
        if (source == null) {
            return null;
        }

        final ContentDTO contentDTO = new ContentDTO();
        contentDTO.setId(source.getId());
        contentDTO.setCreatedAt(source.getCreatedAt());
        contentDTO.setUpdatedAt(source.getUpdatedAt());
        contentDTO.setRating(source.getRating());
        contentDTO.setTitle(source.getTitle());
        contentDTO.setWhoIsItFor(source.getWhoIsItFor());
        contentDTO.setDescription(source.getDescription());
        return contentDTO;
    }
}
