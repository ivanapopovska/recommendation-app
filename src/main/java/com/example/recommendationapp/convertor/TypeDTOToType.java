package com.example.recommendationapp.convertor;

import com.example.recommendationapp.DTO.TypeDTO;
import com.example.recommendationapp.model.Type;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class TypeDTOToType implements Converter<TypeDTO, Type> {

    @Synchronized
    @Nullable
    @Override
    public Type convert(TypeDTO source) {
        if(source== null) {
            return null;
        }
        final Type type = new Type();
        type.setId(source.getId());
        type.setTypeName(source.getTypeName());
        return type;
    }
}
