package com.example.recommendationapp.convertor;

import com.example.recommendationapp.DTO.UserDTO;
import com.example.recommendationapp.model.User;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class UserDTOToUser implements Converter<UserDTO, User> {

    @Synchronized
    @Nullable
    @Override
    public User convert(UserDTO source) {
        if(source == null) {
            return null;
        }
        final User user = new User();
        user.setId(source.getId());
        user.setFullName(source.getFullName());
        return user;
    }
}
