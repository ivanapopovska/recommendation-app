package com.example.recommendationapp.convertor;

import com.example.recommendationapp.DTO.ContentDTO;
import com.example.recommendationapp.model.Content;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ContentDTOToContent implements Converter<ContentDTO, Content> {

    @Synchronized
    @Nullable
    @Override
    public Content convert(ContentDTO source) {
        if (source == null) {
            return null;
        }

        final Content content = new Content();
        content.setId(source.getId());
        content.setCreatedAt(source.getCreatedAt());
        content.setUpdatedAt(source.getUpdatedAt());
        content.setRating(source.getRating());
        content.setTitle(source.getTitle());
        content.setWhoIsItFor(source.getWhoIsItFor());
        content.setDescription(source.getDescription());
        return content;
    }
}
