package com.example.recommendationapp.convertor;

import com.example.recommendationapp.DTO.TypeDTO;
import com.example.recommendationapp.model.Type;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class TypeToTypeDTO implements Converter<Type, TypeDTO> {

    @Synchronized
    @Nullable
    @Override
    public TypeDTO convert(Type source) {
        if(source ==  null) {
            return null;
        }

        final TypeDTO typeDTO = new TypeDTO();
        typeDTO.setId(source.getId());
        typeDTO.setTypeName(source.getTypeName());
        return typeDTO;
    }
}
