package com.example.recommendationapp.convertor;

import com.example.recommendationapp.DTO.TagDTO;
import com.example.recommendationapp.model.Tag;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class TagDTOToTag implements Converter<TagDTO, Tag> {

    @Synchronized
    @Nullable
    @Override
    public Tag convert(TagDTO source) {
        if(source == null) {
            return null;
        }
        final Tag tag = new Tag();
        tag.setId(source.getId());
        tag.setTagName(source.getTagName());
        return tag;
    }
}
