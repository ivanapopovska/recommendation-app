package com.example.recommendationapp.convertor;

import com.example.recommendationapp.DTO.UserDTO;
import com.example.recommendationapp.model.User;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class UserToUserDTO implements Converter<User, UserDTO> {

    @Synchronized
    @Nullable
    @Override
    public UserDTO convert(User source) {
        if(source == null) {
            return null;
        }
        final UserDTO userDTO = new UserDTO();
        userDTO.setId(source.getId());
        userDTO.setFullName(source.getFullName());
        return userDTO;
    }
}
