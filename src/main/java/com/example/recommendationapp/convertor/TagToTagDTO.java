package com.example.recommendationapp.convertor;

import com.example.recommendationapp.DTO.TagDTO;
import com.example.recommendationapp.model.Tag;
import lombok.AllArgsConstructor;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class TagToTagDTO implements Converter<Tag, TagDTO> {

    @Synchronized
    @Nullable
    @Override
    public TagDTO convert(Tag source) {
        if(source == null) {
            return null;
        }
        final TagDTO tagDTO = new TagDTO();
        tagDTO.setId(source.getId());
        tagDTO.setTagName(source.getTagName());
        return tagDTO;
    }
}
